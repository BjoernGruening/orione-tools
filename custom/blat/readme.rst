BLAT wrapper
============

Install
-------

This BLAT wrapper let you install `BLAT`_ tool dependency automatically. In order to do this, you need to:

- have Galaxy release_2013.11.04 or later;
- respect BLAT licensing: BLAT source and executables are freely available for academic, nonprofit and personal use. Commercial licensing information is available on the `Kent Informatics website`_.

.. _BLAT: http://genome.ucsc.edu/FAQ/FAQblat.html
.. _Kent Informatics website: http://www.kentinformatics.com/

Version history
---------------

- Unreleased: Update Orione citation.
- Release 5 (blat_wrapper 0.3): Correct requirement for blat version 35x1 .
- Release 4 (blat_wrapper 0.3): Depend on `package_blat_35x1`_ instead of compiling BLAT (requires Galaxy release_2013.11.04 or later).
- Release 3 (blat_wrapper 0.3): Use column number for installed database sorting.
- Release 2 (blat_wrapper 0.3): Add the possibility to use locally installed FASTA or 2bit files as database source.
- Release 1 (blat_wrapper 0.2): Add version_command element. Add test case from `Joachim Jacob's blat wrapper`_. Add readme section to tool_dependencies.xml .
- Release 0 (blat_wrapper 0.2): Initial release in the Tool Shed.

.. _Joachim Jacob's blat wrapper: http://testtoolshed.g2.bx.psu.edu/view/joachim-jacob/blat
.. _package_blat_35x1: http://toolshed.g2.bx.psu.edu/view/iuc/package_blat_35x1

Development
-----------

Development is hosted at https://bitbucket.org/crs4/orione-tools . Contributions and bug reports are very welcome!
