<tool id="sspace" name="SSPACE" version="1.0.7">
  <description>scaffolder</description>
  <requirements>
    <requirement type="package" version="1.0.0">bowtie</requirement>
    <requirement type="package" version="2.1">sspace</requirement>
  </requirements>

  <command>
    echo "lib1 $reads1 $reads2 $insert $error $orientation" &gt; libraryfile &amp;&amp;
    perl `which SSPACE_Basic_v2.0.pl` -l libraryfile -s $contigs
    #if $exten
      -x 1
    #end if
    #if str($minoverlap)
      -m $minoverlap
    #end if
    #if str($numofreads)
      -o $numofreads
    #end if
    #if str($max_trim)
      -t $max_trim
    #end if
    #if $unpaired
      -u $unpaired
    #end if
    #if str($min_base_ratio)
      -r $min_base_ratio
    #end if
    #if str($minlink)
      -k $minlink
    #end if
    #if str($maxratio)
      -a $maxratio
    #end if
    #if str($contigoverlap)
      -n $contigoverlap
    #end if
    #if str($mincontig)
      -z $mincontig
    #end if
    -T \${GALAXY_SLOTS:-1} -b sspace
  </command>

  <inputs>
    <param name="contigs" type="data" format="fasta" label="Contigs FASTA file (-s)" />
    <param name="reads1" type="data" format="fasta,fastq" label="Paired-end reads 1" help="FASTA or FASTQ format" />
    <param name="reads2" type="data" format="fasta,fastq" label="Paired-end reads 2" help="FASTA or FASTQ format" />
    <param name="insert" type="integer" value="" label="Insert size">
      <validator type="in_range" min="1" />
    </param>
    <param name="error" type="float" min="0" max="1" value="" label="Variability (e.g. 0.25 for 25%)" />
    <param name="orientation" type="select" label="Orientation">
      <option value="FF">FF</option>
      <option value="FR" selected="true">FR</option>
      <option value="RF">RF</option>
      <option value="RR">RR</option>
    </param>
    <param name="exten" type="boolean" checked="true" label="Extend the contigs using the paired reads (-x)" help="Uncheck for scaffolding only" />
    <param name="minoverlap" type="integer" optional="true" min="10" max="50" value="35" label="Extension: minimum number of overlapping bases with the seed/contig during overhang consensus build up (-m)" help="E.g. 32-35 for 36bp reads" />
    <param name="numofreads" type="integer" optional="true" value="8" label="Extension: minimum number of reads needed to call an extension (-o)" help="Higher numbers increase reliability of the extension">
      <validator type="in_range" min="1" />
    </param>
    <param name="max_trim" type="integer" optional="true" value="0" label="Extension: maximum number of bases to trim on the contig end when all possibilities have been exhausted for an extension (-t)">
      <validator type="in_range" min="0" />
    </param>
    <param name="unpaired" type="data" format="fasta,fastq" optional="true" label="Extension: unpaired reads (-u, optional)" help="FASTA or FASTQ format" />
    <param name="min_base_ratio" type="float" optional="true" min="0" max="1" value="0.9" label="Extension: minimum base ratio used to accept a overhang consensus base (-r)" />
    <param name="minlink" type="integer" optional="true" value="5" label="Scaffolding: minimum number of links (read pairs) to compute scaffold (-k)">
      <validator type="in_range" min="0" />
    </param>
    <param name="maxratio" type="float" optional="true" min="0" max="1" value="0.7" label="Scaffolding: maximum link ratio between two best contig pairs (-a)" help="Higher values lead to less accurate scaffolding" />
    <param name="contigoverlap" type="integer" optional="true" value="30" label="Scaffolding: minimum overlap required between contigs to merge adjacent contigs in a scaffold (-n)">
      <validator type="in_range" min="0" />
    </param>
    <param name="mincontig" type="integer" optional="true" value="0" label="Scaffolding: minimum contig length used for scaffold (-z)" help="Filters out contigs below this length. Leave empty or set to 0 for no filtering">
      <validator type="in_range" min="0" />
    </param>
  </inputs>

  <outputs>
    <data name="finalevidence" format="txt" label="${tool.name} on ${on_string}: final evidence" from_work_dir="sspace.final.evidence" />
    <data name="finalscaffolds" format="fasta" label="${tool.name} on ${on_string}: final scaffolds" from_work_dir="sspace.final.scaffolds.fasta" />
    <data name="logfile" format="txt" label="${tool.name} on ${on_string}: log" from_work_dir="sspace.logfile.txt" />
    <data name="summaryfile" format="txt" label="${tool.name} on ${on_string}: summary" from_work_dir="sspace.summaryfile.txt" />
  </outputs>
  <tests>

  </tests>
  <help>
**What it does**

SSPACE is a script able to extend and scaffold pre-assembled contigs using one or more mate pairs or paired-end libraries, or even a combination.

**License and citation**

This Galaxy tool is Copyright © 2012-2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

You can use this tool only if you agree to the license terms of: `SSPACE basic`_.

.. _SSPACE basic: http://www.baseclear.com/landingpages/basetools-a-wide-range-of-bioinformatics-solutions/sspacev12/

If you use this tool, please cite:

- |Cuccuru2014|_
- |Boetzer2011|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |Boetzer2011| replace:: Boetzer, M., Henkel, C. V., Jansen, H. J., Butler, D., Pirovano, W. (2011) Scaffolding pre-assembled contigs using SSPACE. *Bioinformatics* 27(4), 578-579
.. _Boetzer2011: http://bioinformatics.oxfordjournals.org/content/27/4/578
  </help>
  <citations>
    <citation type="doi">10.1093/bioinformatics/btu135</citation>
    <citation type="doi">10.1093/bioinformatics/btq683</citation>
  </citations>
</tool>
