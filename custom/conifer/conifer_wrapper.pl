use strict;
use warnings;
use File::Basename; 
#use Cwd qw( abs_path );
use File::Path qw(make_path remove_tree);
my $dir=$ENV{'CONIFER_PATH'};
my $input=$ARGV[0];
my $calls=$ARGV[1];
my $win=$ARGV[2];
my $html=$ARGV[3];
my $folder=$ARGV[4];
make_path($folder);

#reformat input file to make it conifer compatible
system("awk '{print \$5,\$1,\$2,\$3,\$4}' OFS=\"\t\" $calls > temporary");
system("mv temporary $calls");

#run conifer 
system ("python ".$dir."/conifer.py plotcalls --input $input --calls $calls --window $win --outputdir $folder  2>&1");
print "python ".$dir."/conifer.py plotcalls --input $input --calls $calls --window $win --outputdir $folder  2>&1";

#set html
#print "$contra_output - $working_dir\n";
open(HTML, ">$html");
print HTML "<html><head><title>Conifer: Copy Number Analysis for Targeted Resequencing</title></head><body><h3>Conifer Output Files:</h3><p><ul>\n";
opendir(DIR, $folder);
my @FILES= grep { /png$/ }  readdir(DIR); 
closedir(DIR);
foreach my $file (@FILES) 
{
   print HTML "<li><a href=$file>$file</a><img src=\"$file\" height=\"50\" width=\"100\"></li>\n";		
}
print HTML "</ul></p></body></html>\n";
close(HTML);



