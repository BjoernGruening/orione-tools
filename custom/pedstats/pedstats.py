# -*- coding: utf-8 -*-
"""
PedStats
version 0.1 (andrea.pinna@crs4.it)
"""

import optparse
import os
import shutil
import subprocess
import sys
import time


def __main__():

    ex_path = 'pedstats'

    # load arguments
    print 'Parsing PedStats input options...'
    parser = optparse.OptionParser()
    
    parser.add_option('--data_files', action='append', dest='data_files')
    parser.add_option('--pedigree_files', action='append', dest='pedigree_files')
    parser.add_option('--ibd_file', dest='ibd_file')
    parser.add_option('--missing_value_code', dest='missing_value_code')
    
    parser.add_option('--ignore_mendelian_errors', action="store_true", dest='ignore_mendelian_errors')
    parser.add_option('--chromosome_x', action="store_true", dest='chromosome_x')
    parser.add_option('--trim', action="store_true", dest='trim')

    parser.add_option('--hardy_weinberg', action="store_true", dest='hardy_weinberg')
    parser.add_option('--show_all', action="store_true", dest='show_all')
    parser.add_option('--cutoff', dest='cutoff')

    parser.add_option('--check_unrelated', action="store_true", dest='check_unrelated')
    parser.add_option('--check_all', action="store_true", dest='check_all')
    parser.add_option('--check_founders', action="store_true", dest='check_founders')

    parser.add_option('--age', dest='age')
    parser.add_option('--birth', dest='birth')

    parser.add_option('--min_gap', dest='min_gap')
    parser.add_option('--max_gap', dest='max_gap')
    parser.add_option('--sib_gap', dest='sib_gap')

    parser.add_option('--pairs', action="store_true", dest='pairs')
    parser.add_option('--rewrite_pedigree', action="store_true", dest='rewrite_pedigree')
    parser.add_option('--verbose', action="store_true", dest='verbose')
    parser.add_option('--marker_tables', action="store_true", dest='marker_tables')

    parser.add_option('--by_family', action="store_true", dest='by_family')
    parser.add_option('--by_sex', action="store_true", dest='by_sex')

    parser.add_option('--pdf', action="store_true", dest='pdf')
    parser.add_option('--marker_pdf', action="store_true", dest='marker_pdf')
    parser.add_option('--family_pdf', action="store_true", dest='family_pdf')
    parser.add_option('--trait_pdf', action="store_true", dest='trait_pdf')
    parser.add_option('--aff_pdf', action="store_true", dest='aff_pdf')

    parser.add_option('--min_genos', dest='min_genos')
    parser.add_option('--min_phenos', dest='min_phenos')
    parser.add_option('--min_covariates', dest='min_covariates')
    parser.add_option('--affected_for', dest='affected_for')
    
    parser.add_option('--logfile', dest='logfile')
    parser.add_option('--output_id', dest='output_id')
    parser.add_option('--new_file_path', dest='new_file_path')

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build PedStats command to be executed

    data_files = '-d '
    for item in options.data_files:
        data_files += '%s,' % (item)
    data_files = data_files[:-1]
    
    pedigree_files = '-p '
    for item in options.pedigree_files:
        pedigree_files += '%s,' % (item)
    pedigree_files = pedigree_files[:-1]

    ibd_file = '-i %s' % (options.ibd_file) if options.ibd_file is not None else ''

    missing_value_code = '-x %s' % (options.missing_value_code) if options.missing_value_code is not None else ''

    ignore_mendelian_errors = '--ignoreMendelianErrors' if options.ignore_mendelian_errors is not None else ''
    chromosome_x = '--ChromosomeX' if options.chromosome_x is not None else ''
    trim = '--trim' if options.trim is not None else ''

    hardy_weinberg = '--hardyWeinberg' if options.hardy_weinberg is not None else ''
    show_all = '--showAll' if options.show_all is not None else ''
    cutoff = '--cutoff %s' % (options.cutoff) if options.cutoff is not None else ''

    check_unrelated = '--checkUnrelated' if options.check_unrelated is not None else ''
    check_all = '--checkAll' if options.check_all is not None else ''
    check_founders = '--checkFounders' if options.check_founders is not None else ''

    age = '--age %s' % (options.age) if options.age is not None else ''
    birth = '--birth %s' % (options.birth) if options.birth is not None else ''

    min_gap = '--minGap %s' % (options.min_gap) if options.min_gap is not None else ''
    max_gap = '--maxGap %s' % (options.max_gap) if options.max_gap is not None else ''
    sib_gap = '--sibGap %s' % (options.sib_gap) if options.sib_gap is not None else ''

    pairs = '--pairs' if options.pairs is not None else ''
    rewrite_pedigree = '--rewritePedigree' if options.rewrite_pedigree is not None else ''
    verbose = '--verbose' if options.verbose is not None else ''
    marker_tables = '--markerTables' if options.marker_tables is not None else ''

    by_family = '--byFamily' if options.by_family is not None else ''
    by_sex = '--bySex' if options.by_sex is not None else ''

    pdf = '--pdf' if options.pdf is not None else ''
    marker_pdf = '--markerPDF' if options.marker_pdf is not None else ''
    family_pdf = '--familyPDF' if options.family_pdf is not None else ''
    trait_pdf = '--traitPDF' if options.trait_pdf is not None else ''
    aff_pdf = '--affPDF' if options.aff_pdf is not None else ''

    min_genos = '--minGenos %s' % (options.min_genos) if options.min_genos is not None else ''
    min_phenos = '--minPhenos %s' % (options.min_phenos) if options.min_phenos is not None else ''
    min_covariates = '--minCovariates %s' % (options.min_covariates) if options.min_covariates is not None else ''
    affected_for = '--affectedFor %s' % (options.affected_for) if options.affected_for is not None else ''

    # output file(s)
    logfile = options.logfile
    output_id = options.output_id
    new_file_path = options.new_file_path    


    # Build PedStats command
    cmd = '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (ex_path, data_files, pedigree_files, ibd_file, missing_value_code, ignore_mendelian_errors, chromosome_x, trim, hardy_weinberg, show_all, cutoff, check_unrelated, check_all, check_founders, age, birth, min_gap, max_gap, sib_gap, pairs, rewrite_pedigree, verbose, marker_tables, by_family, by_sex, pdf, marker_pdf, family_pdf, trait_pdf, aff_pdf, min_genos, min_phenos, min_covariates, affected_for)
    print '\nPedStats command to be executed:\n %s' % (cmd)

    # Execution of PedStats
    print 'Executing PedStats...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True) # need to redirect stderr because PedStats writes some logging info there

        cwd = os.getcwd()
        files = sorted([ f for f in os.listdir(cwd) if (os.path.isfile(f) and f.startswith('pedstats')) ])
        it = 0
        for f in files:
            file_extension = f.split('.')[-1]
            if file_extension == 'pdf':
                file_type = 'pdf'
            else:
                file_type = 'txt'
            new_file = "%s/%s_%s_%s_%s_%s" % ( new_file_path, 'primary', output_id, f, 'visible', file_type )
            s = subprocess.check_call(['mv', f, new_file])
            print "Successfully moved %s to %s: return %s" % (f, new_file, s)
            it = it + 1
        print "Successfully moved %d of %d files!" % (it, len(files))


        
    finally:

        if log != sys.stdout:
            log.close()

    print 'PedStats executed!'
    

if __name__ == "__main__":
    __main__()
