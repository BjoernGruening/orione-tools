# -*- coding: utf-8 -*-
"""
Plink
version 0.1 (andrea.pinna@crs4.it)
"""

import optparse
import os
import shutil
import subprocess
import sys
import time


def __main__():

    ex_path = 'plink'

    # load arguments
    print 'Parsing Plink input options...'
    parser = optparse.OptionParser()

    parser.add_option('--input_type', dest='input_type')
    parser.add_option('--ped_file', dest='ped_file')
    parser.add_option('--map_file', dest='map_file')
    parser.add_option('--lped_path', dest='lped_path')
    parser.add_option('--lped_basefile', dest='lped_basefile')
    parser.add_option('--pbed_path', dest='pbed_path')
    parser.add_option('--pbed_basefile', dest='pbed_basefile')

    parser.add_option('--extract_file', dest='extract_file')
    parser.add_option('--read_genome_file', dest='read_genome_file')
    parser.add_option('--within_file', dest='within_file')

    parser.add_option('--assoc', action="store_true", dest='assoc')
    parser.add_option('--missing', action="store_true", dest='missing')
    parser.add_option('--cluster', action="store_true", dest='cluster')
    parser.add_option('--mh', action="store_true", dest='mh')
    parser.add_option('--allow_no_sex', action="store_true", dest='allow_no_sex')
    parser.add_option('--hardy', action="store_true", dest='hardy')
    parser.add_option('--logistic', action="store_true", dest='logistic')
    parser.add_option('--sex', action="store_true", dest='sex')
    parser.add_option('--genome', action="store_true", dest='genome')
    parser.add_option('--make_bed', action="store_true", dest='make_bed')

    parser.add_option('--maf', dest='maf')
    parser.add_option('--mind', dest='mind')
    parser.add_option('--hwe', dest='hwe')
    parser.add_option('--geno', dest='geno')
    parser.add_option('--ppc', dest='ppc')

    parser.add_option('--indep_window_size', dest='indep_window_size')
    parser.add_option('--indep_window_step', dest='indep_window_step')
    parser.add_option('--indep_threshold', dest='indep_threshold')
    parser.add_option('--indep_pairwise_window_size', dest='indep_pairwise_window_size')
    parser.add_option('--indep_pairwise_window_step', dest='indep_pairwise_window_step')
    parser.add_option('--indep_pairwise_threshold', dest='indep_pairwise_threshold')

    parser.add_option('--mds_plot', dest='mds_plot')

    parser.add_option('--output', dest='logfile')
    parser.add_option('--output_id', dest='output_id')
    parser.add_option('--new_file_path', dest='new_file_path')

    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build Plink command to be executed

    print options.input_type
    if options.input_type == 'pedmap':
        input_files = '--ped %s --map %s' % (options.ped_file,  options.map_file)
    elif options.input_type == 'lped':
        lped_files = os.path.join(options.lped_path,options.lped_basefile)
        input_files = '--file %s' % lped_files
    elif options.input_type == 'bped':
        pbed_files = os.path.join(options.pbed_path,options.pbed_basefile)
        input_files = '--bfile %s' % pbed_files
    else:
        print 'Error!!! No input file specified!'

    extract_file = '--extract %s' % (options.extract_file) if options.extract_file is not None else ''
    read_genome_file = '--read-genome %s' % (options.read_genome_file) if options.read_genome_file is not None else ''
    within_file = '--within %s' % (options.within_file) if options.within_file is not None else ''


    assoc = '--assoc' if options.assoc is not None else ''
    missing = '--missing' if options.missing is not None else ''
    cluster = '--cluster' if options.cluster is not None else ''
    mh = '--mh' if options.mh is not None else ''
    allow_no_sex = '--allow-no-sex' if options.allow_no_sex is not None else ''
    hardy = '--hardy' if options.hardy is not None else ''
    logistic = '--logistic' if options.logistic is not None else ''
    sex = '--sex' if options.sex is not None else ''
    genome = '--genome' if options.genome is not None else ''
    make_bed = '--make-bed' if options.make_bed is not None else ''

    maf = '--maf %s' % (options.maf) if options.maf is not None else ''
    mind = '--mind %s' % (options.mind) if options.mind is not None else ''
    hwe = '--hwe %s' % (options.hwe) if options.hwe is not None else ''
    geno = '--geno %s' % (options.geno) if options.geno is not None else ''
    ppc = '--ppc %s' % (options.ppc) if options.ppc is not None else ''

    if options.indep_window_size and options.indep_window_step and options.indep_threshold:
        indep = '--indep %s %s %s' % (options.indep_window_size, options.indep_window_step, options.indep_threshold)
    else:
        indep = ''

    if options.indep_pairwise_window_size and options.indep_pairwise_window_step and options.indep_pairwise_threshold:
        indep_pairwise = '--indep-pairwise %s %s %s' % (options.indep_pairwise_window_size, options.indep_pairwise_window_step, options.indep_pairwise_threshold)
    else:
        indep_pairwise = ''

    mds_plot = '--mds-plot %s' % (options.mds_plot) if options.mds_plot is not None else ''

    no_web = '--noweb'
    out = '--out plink'

    # output file(s)
    logfile = options.logfile
    output_id = options.output_id
    new_file_path = options.new_file_path   


    # Build Plink command
    cmd = '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (ex_path, input_files, extract_file, read_genome_file, within_file, assoc, missing, cluster, mh, allow_no_sex, hardy, logistic, sex, genome, make_bed, maf, mind, hwe, geno, ppc, indep, indep_pairwise, mds_plot, no_web, out)
    print '\nPlink command to be executed:\n %s' % (cmd)

    # Execution of Plink
    print 'Executing Plink...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True) # need to redirect stderr because Plink writes some logging info there

        cwd = os.getcwd()
        files = sorted([ f for f in os.listdir(cwd) if (os.path.isfile(f) and f.startswith('plink') and not f.endswith('log')) ])
        it = 0
        for f in files:
            file_extension = f.split('.')[-1]
            if file_extension == 'pdf':
                file_type = 'pdf'
            elif file_extension == 'bed':
                file_type = 'bed'
            else:
                file_type = 'txt'
            new_file = "%s/%s_%s_%s_%s_%s" % ( new_file_path, 'primary', output_id, f, 'visible', file_type )
            s = subprocess.check_call(['mv', f, new_file])
            print "Successfully moved %s to %s: return %s" % (f, new_file, s)
            it = it + 1
        print "Successfully moved %d of %d files!" % (it, len(files))

    finally:

        if log != sys.stdout:
            log.close()
    print 'Plink executed!'
    

if __name__ == "__main__":
    __main__()
