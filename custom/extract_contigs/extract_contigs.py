# -*- coding: utf-8 -*-
"""
Extract contigs from a draft bacterial genome if longer than the specified threshold
"""

import optparse
import re

def __main__():
    parser = optparse.OptionParser()
    parser.add_option('-d', dest='draft', help='draft file')
    parser.add_option('-t', dest='threshold', type='int', default=200, help='min length of retained contigs [200]')
    parser.add_option('-o', dest='outname', help='output contigs')
    parser.add_option('-q', dest='hqoutname', help='output high quality contigs')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    infilename = options.draft
    threshold = options.threshold
    outname = options.outname
    hqoutname = options.hqoutname
    contig = re.compile('[acgtACGTryswkmbdhvRYSWKMBDHV]+')
    hqcontig = re.compile('[ACGT]+')
    fasta = ''
    with open(infilename, 'r') as infile:
        infile.next() # skip header
        try:
            while True:
                row = infile.next().strip()
                fasta += row
        except StopIteration:
            fasta.replace("\n", "")

    contigiterator = contig.finditer(fasta)
    hqcontigiterator = hqcontig.finditer(fasta)
    counter = 1
    with open(outname, 'w') as out:
        for contig in contigiterator:
            length = len(contig.group())
            if length >= threshold:
                header = ">contig" + str(counter) + '_S' + str(contig.span()[0]) + '_E' + str(contig.span()[1]) + '_L' + str(length)
                sequence = contig.group()
                out.write(header + '\n' + sequence + '\n')
                counter += 1
    counter = 1
    with open(hqoutname, 'w') as out:
        for contig in hqcontigiterator:
            length = len(contig.group())
            if length >= threshold:
                header = ">contig" + str(counter) + '_S' + str(contig.span()[0]) + '_E' + str(contig.span()[1]) + '_L' + str(length)
                sequence = contig.group()
                out.write(header + '\n' + sequence + '\n')
                counter += 1


if __name__ == "__main__":
    __main__()
