# -*- coding: utf-8 -*-
"""
Glimmer --> glimmer3
version 0.2 (andrea.pinna@crs4.it)
"""

import optparse
import shutil
import subprocess
import sys

def __main__():
    # load arguments
    print 'Parsing input options...'
    parser = optparse.OptionParser()
    parser.add_option('--glSequence', dest='sequence', help='')
    parser.add_option('--glIcm', dest='icm', help='')
    #parser.add_option('--glPrefix', dest='prefix', help='')
    parser.add_option('--glStartCodons', dest='start_codons', help='')
    parser.add_option('--glRbsPwm', dest='rbs_pwm', help='')
    parser.add_option('--glGcPercent', dest='gc_percent', type='float', help='')
    parser.add_option('--glEntropy', dest='entropy', help='')
    parser.add_option('--glFirstCodon', action='store_true', dest='first_codon', help='')
    parser.add_option('--glGeneLen', dest='gene_len', type='int', help='')
    parser.add_option('--glIgnore', dest='ignore', help='')
    parser.add_option('--glLinear', action='store_true', dest='linear', help='')
    parser.add_option('--glOrfCoords', dest='orf_coords', help='')
    parser.add_option('--glSeparateGenes', action='store_true', dest='separate_genes', help='')
    parser.add_option('--glMaxOverlap', dest='max_olap', type='int', help='')
    parser.add_option('--glStartProbs', dest='start_probs', help='')
    parser.add_option('--glIgnoreScoreLen', dest='ignore_score_len', type='int', help='')
    parser.add_option('--glNoIndep', action='store_true', dest='no_indep', help='')
    parser.add_option('--glThreshold', dest='threshold', type='int', help='')
    parser.add_option('--glExtend', action='store_true', dest='extend', help='')
    parser.add_option('--glTransTable', dest='trans_table', type='int', help='')
    parser.add_option('--glStopCodons', dest='stop_codons', help='')
    parser.add_option('--glDetail', dest='detail', help='')
    parser.add_option('--glPredict', dest='predict', help='')
    parser.add_option('--logfile', dest='logfile', help='')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    # build Glimmer3 command to be executed
    # sequence file
    sequence = options.sequence
    # icm file
    icm = options.icm
    # prefix (not needed)
    prefix = 'prefix'
    # start codons
    start_codons = '--start_codons %s' % (options.start_codons) if options.start_codons else ''
    # rbs_pwm
    rbs_pwm = '--rbs_pwm %s' % (options.rbs_pwm) if options.rbs_pwm else ''
    # gc percentage
    gc_percent = '--gc_percent %s' % (options.gc_percent) if options.gc_percent is not None else ''
    # entropy
    entropy = "--entropy '%s'" % (options.entropy) if options.entropy else ''
    # first_codon
    first_codon = '--first_codon' if options.first_codon else ''
    # gene length
    gene_len = '--gene_len %d' % (options.gene_len) if options.gene_len is not None else ''
    # ignore
    ignore = '--ignore %s' % (options.ignore) if options.ignore else ''
    # linear
    linear = '--linear' if options.linear else ''
    # orf_coords
    orf_coords = '--orf_coords %s' % (options.orf_coords) if options.orf_coords else ''
    # separate genes
    separate_genes = '--separate_genes' if options.separate_genes else ''
    # max overlap
    max_olap = '--max_olap %d' % (options.max_olap) if options.max_olap is not None else ''
    # start probs
    start_probs = '--start_probs %s' % (options.start_probs) if options.start_probs else ''
    # ignore score length
    ignore_score_len = '--ignore_score_len %d' % (options.ignore_score_len) if options.ignore_score_len is not None else ''
    # no indep
    no_indep = '--no_indep' if options.no_indep else ''
    # threshold
    threshold = '--threshold %d' % (options.threshold) if options.threshold is not None else ''
    # extend
    extend = '--extend' if options.extend else ''
    # trans table
    trans_table = '--trans_table %d' % (options.trans_table) if options.trans_table is not None else ''
    # stop codons
    stop_codons = '--stop_codons %s' % (options.stop_codons) if options.stop_codons else ''
    logfile = options.logfile

    # Build Glimmer3 command
    cmd = 'glimmer3 %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (start_codons, rbs_pwm, gc_percent, entropy, first_codon, gene_len, ignore, linear, orf_coords, separate_genes, max_olap, start_probs, ignore_score_len, no_indep, threshold, extend, trans_table, stop_codons, sequence, icm, prefix)
    print '\nGlimmer3 command to be executed:\n %s' % (cmd)

    print 'Executing Glimmer3...'
    log = open(logfile, 'w') if logfile else sys.stdout
    try:
        subprocess.check_call(cmd, stdout=log, stderr=subprocess.STDOUT, shell=True)
    finally:
        if log != sys.stdout:
            log.close()
    print 'Glimmer3 executed!'

    shutil.move(prefix + ".detail", options.detail)
    shutil.move(prefix + ".predict", options.predict)


if __name__ == "__main__":
    __main__()
